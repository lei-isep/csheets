\chapter{Introduction}
\label{ch:introducao}

This document is a guide for LAPR4, edition of 2016/17.

It is intended that this document be sufficiently complete in order to help
students to organize their work. It is however very likely that new issues
may arise that are not covered by the current version of the document. In this case
the student must make use of Moodle and Hipchat forums dedicated to the project and LAPR4 or
contact directly any of the LAPR4 teachers.

With regard to this document all suggestions and criticisms can be sent to
atb@isep.ipp.pt.

\textbf{It is very important that all students read the entirety of this document
before starting the project.}

\section{Cleansheets}

The aims of the LAPR4 course are dual:
\begin{enumerate}
	\item Introduce students to software project management, specially in agile approaches;
	\item Introduce students to an ongoing software development project where they need to work in a large team with an agile approach, applying best practices and techniques.
\end{enumerate}

In this context, Cleansheets is adopted to address item 2. Using an artifact to support teaching is not new. You can find further information in the description of the pedagogical pattern "Adopt an Artifact" in the book "Pedagogical patterns: Advice for educators" \cite{ped_patterns}.\newline

\textit{"CleanSheets is the first spreadsheet application that is both extensible and platform-independent. It features a formula language that closely resembles that of Microsoft Excel, and extensions for aiding end-user programmers develop correct spreadsheets"}. This is how the application is described by its original author, Einar Pehrson.

You can contact the original author by using the following email \texttt{\href{mailto:einar.pehrson@gmail.com}{einar.pehrson@gmail.com}}.

You can find professional information about the original author at \url{https://www.linkedin.com/in/einarp}.

You can find the original version of Cleansheets at \url{http://csheets.sourceforge.net}.\newline

Each team will have its own repository with a version of Cleansheets. You can find the repository for the base version of Cleansheets, edition 2017, in \url{https://bitbucket.org/lei-isep/csheets17} \cite{rep_cleansheets}.\newline

If you wish to learn more about spreadsheets we recommend the following reading: "Spreadsheet Technology" by Peter Sestoft \cite{spreadsheettechnology}, available at \url{http://www.itu.dk/people/sestoft/funcalc/ITU-TR-2011-142.pdf}.

\section{Structure of the Document}

Chapter \ref{ch:support_tools} describes how to execute the main operations regarding Bitbucket, specially the \texttt{issues} that will be used during the project. The chapter also contains briefly explications regarding Jira and Hipchat.

JIRA is installed in a DEI server and we hope it will function smoothly. However, problems may happen (we hope not). If problems happen, students should report the problems as soon as possible to the teachers so that the problems can be solved as briefly as possible.


Chapter \ref{ch:dev_tools} explains the use of the development tools. Basically it describes the Netbeans IDE and using Sourcetree (a GUI interface to git).

The project is specially prepared to be developed with the Netbeans IDE. However it is possible to use other Java IDEs, like, for instance, Eclipse.

The Sourcetree tool is available for OSX and Windows. If you are using Linux there are several alternatives to Sourcetree, one of them is SmartGit. In Windows you can also use TortoiseGit. You can also operate all the git related tasks from inside Netbeans (or the other IDEs).

Chapter \ref{ch:tasks} explains the fundamental tasks that should be realized during the project. It also contains some proposals on how students should organize their individual and team work.

Chapter \ref{ch:assessment} explains the rules and principles of the assessment.

Chapter \ref{ch:requirements} presents all the requirements of the project. 

Chapter \ref{ch:teams} presents the teams and its weekly timetable.

\section{Project Context}

The LAPR4 course has similar goals as the other LAPR courses of the LEI undergraduate program. Specifics about its aims and outcomes are described in its FUC ("Ficha de Unidade Curricular").
 
The LAPR4 main features can be described as:

\begin{itemize}
	\item Strong emphasis on the software development process justified because LAPR4 is mainly based on contents from the EAPLI course. In this context, it is valued the process used to obtain the product (software) over the product itself. It is expected that the student is able to apply best practices studied in EAPLI, applying an agile and iterative process. In addition to the principles of object oriented analysis, design, coding and testing it is also expected that the student be able to use a distributed version control system as well as tools to support the management of agile projects (e.g., Jira);
	\item Students will work in a ongoing software project, integrating a large team and reusing work previously developed by others;
	\item According to the concepts and aims of the Project Management Module of LAPR4 it is expected that the student demonstrates project planning and management skills applying agile principles and using project management tools;
	\item The development of the project will require the application of concepts, techniques and practices from other courses of the semester: SCOMP, RCOMP and LPROG. In particular, it is intended that the student apply techniques of interprocess communication (and networking); parallel programming techniques; synchronization techniques; design of grammars and use of generated parsers; manipulation of XML and related technologies.
\end {itemize}

Based on the previous features of LAPR4, the 2016/17 edition of the project will:
\begin{itemize} 
	\item be developed by teams that correspond to the Lab class (i.e., "Turma PL");
	\item use Git \cite{git};
 	\item use Bitbucket to maintain the central repository/copy of the repository of the team \cite{bitbucket}. Each team will have a Bitbucket repository already setup;
	\item use Jira for agile project management. Each team will have a Jira project already setup;
 	\item require the development of new features in Cleansheets. Each team will have a copy of Cleansheets already setup in the Bitbucket repository;
	\item use Hipchat as a communication tool between team members. Each team will have a room in Hipchat already setup.
\end{itemize}

The next section describes some highlights regarding the organization of the project.

\section{Team Organization}

Each team of LAPR4 corresponds to a lab class. Usually all the students enrolled in LAPR4 will have an account in Bitbucket, Jira and Hipchat. These accounts must have the ISEP email as the main email of the account. If you have any problem related to these accounts please email \texttt{\href{mailto:atb@isep.ipp.pt}{atb@isep.ipp.pt}}. \newline

New developments in Cleansheets will be made in three areas: Core/CRM\footnote{Customer Relationship Management}, Languages and IPC\footnote{Inter-process communication}. Project requirements result in work that typically falls into one of these three areas.\newline

Within each area requirements are divided into features (F) and functional increments (FI). 

Normally, each student, in each week (i.e., sprint), must take responsibility in one FI of one of the areas. \textbf{It is mandatory that a student has
work developed in all three areas, one different area each sprint}.\newline

During each week/sprint the team must develop requirements in all three areas with an equitable effort between all the areas. The team must select one scrum master for each sprint. The team may have different scrum masters for each sprint. The scrum master should also be an area leader. Each week the team must also select one area leader for each of the areas. The area leader must be working on a FI in the same area he/she is leader of. Scrum masters and area leaders have priority in the selection of the FI that they will be working on.\newline

Once a feature (F) is started it must be completely developed until the end (that is, the team must complete all the functional increments of the feature). Usually, a team can only select a new feature once it has completed one. As stated before, each feature is already divided in functional increments, each increment should represent one week of work for one person. That is why it is expected that each week a developer works in one functional increment of a feature. A developer can work in more than on FI if he/she and the team decide it is feasible.\newline

\vspace{5 mm}
\begin{bclogo}[couleur=blue!30, arrondi=0.1, logo=\bclampe, ombre=true]{"Red", "Green" and "Blue" Groups} 
Each LAPR4 Section/Class/Team (i.e., "Turma PL") should be divided in three Groups (the Red, Green and Blue groups). Since there will be 3 sprints and 3 areas, each class should be divided in 3 groups (with a similar number of members). This division should take place before the first sprint. Groups should be stable for the duration of the project. In each sprint, each group works on a different area. By the end of the project all teams should have worked in all the three areas.
 \end{bclogo}
\vspace{5 mm}

In the case a functional increment is not done by the end of the sprint the same developer is required to finish it in the next sprint (usually 1 to 2 more days of work are expected to be necessary), working in pair with the next student to take the following functional increment of the same feature. Scrum masters and area leaders must be very attentive to issues that may prevent the completion of functional increments. It is their responsibility to identify these problems and, with the help of all the team, react in order to help solve them. \newline

Figure ~\ref{fig:lapr4_uc1} illustrates the different actors (roles) related to the LAPR4 project.

\begin{figure}[h!]   
  \centering
      \includegraphics[width=1\textwidth]{./diagrams/imgs/lapr4_uc1.png}
  \caption{Actor/Roles in the LAPR4 Project}
  \label{fig:lapr4_uc1}
\end{figure} 

Next it is presented a brief description of the main responsibilities of each role.

%% IMPORTANTE:
%% Os docentes supervisores de cada turma deviam criar o sketchboard a ser usado em cada sala do hipchat.

\begin{itemize}
  \item \textbf{Product Owner}: The product owner is ATB. The first session of each sprint will be with the product owner. The product owner can clarify details about the new features to be developed in Cleansheets. 
  \item \textbf{Agile Coach}: The agile coach of the team is one of the teachers of the Project Management Module of LAPR4. The second session of the sprint is with the Agile Coach. The aim of this session is essentially to tutor students about Scrum and its application during the project. 
  \item \textbf{Supervisor}: The supervisor of the team is a teacher of EAPLI. He is also a reviewer for the Core/CRM area. As supervisor of the team he is the teacher with more contact hours.
  \item \textbf{Reviewer}: Reviewers are teachers of SCOMP, RCOMP, LPROG and EAPLI. They have significant responsibility in the assessment.
  \item \textbf{Developer}: Each student is a developer and is expected to develop at least one feature increment in each sprint. 
  \item \textbf{Scrum Master}: One of the students is also the scrum master. The team may change the scrum master in each sprint. The scrum master should supervise the daily scrums. He/she is also responsible for the sprint demonstration. The scrum master should also be an area leader. The scrum master should coordinate all the team with the help of the area leaders. Scrum masters and area leaders produce one report of the team performance for each sprint.
  \item \textbf{Area Leader}: Each team should also have one area/group leader for each group (Red, Green and Blue). Area leaders must help the scrum master in all activities, particularly in their specific areas. The area leader should coordinate is area/group. Scrum masters and area leaders produce one report of the team performance for each sprint. Each area leader produces a report of the status of the area after the daily meeting.
\end{itemize}

\section{Communication Idiom}

During the project of LAPR4 it is expected that:

\begin{itemize}
	\item The written idiom \textbf{must} be English (used in all produced artifacts and documentation, e.g., wiki, javadoc);
	\item The oral and electronic communication idiom can be English or Portuguese;
\end{itemize}






